<?php

/**
 * @file
 * No Koala! Drupal theme by Ross Kendall - http://rosskendall.com.
 *
 * The No Koala! theme is a simple but elegant single-column theme for 
 * the Drupal content management system.  It's goal is to focus on 
 * accessibility and usability, and should be suitable for a blog or
 * other simple websites.
 *
 * The theme uses XHTML and CSS, and incorporates a CSS dropdown menu
 * that is designed to be integrated with the Drupal menu system.
 */

/*
Copyright (C) 2006  Ross Kendall - http://rosskendall.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
http://www.gnu.org/copyleft/gpl.html
*/

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language; ?>" xml:lang="<?php print $language; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <?php print $head; ?>
  <?php print $styles; ?>
  <script type="text/javascript" src="<?php print $base_path.$directory; ?>/menu.js"></script>
</head>

<body >
<div id="skip"><a href="#start" name="top">Skip to content</a></div>
<div id="drop-shadow-top"></div>
<div id="drop-shadow-sides">
<div id="container">

 <div id="header">
  <?php if ($logo) { ?><a href="<?php print $base_path; ?>" title="<?php print $site_name.' '.t('Home Page');?>"><img id="logo" src="<?php print $logo; ?>" alt="<?php print $site_name.' '.t('Logo');?>" /></a><?php } ?>

  <h1 id="site-name"><a href="<?php print $base_path; ?>" title="<?php print $site_name.' '.t('Home Page');?>"><?php print $site_name ?></a></h1>

  <?php if ($site_slogan) { ?><div id="site-description"><?php print $site_slogan; ?></div><?php } ?>
  <?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>

 </div><!--/#header-->

 <div id="nav">
  <?php print $top_menu; ?>

 </div><!--/#nav-->

 <?php if ($sidebar_left): ?>
 <div id="leftcol">
  <?php print $sidebar_left; ?>
 </div><!--/#leftcol-->
 <?php endif; ?>

 <div id="maincol">
  <a name="start"></a><!-- accessibility "Skip to content" anchor -->

  <?php if ($mission) { ?><div id="mission"><?php print $mission; ?></div><?php } ?>

  <?php if (!preg_match('#>Home</a></div>$#', $breadcrumb)) { print $breadcrumb; } ?>

  <?php if ($title) { ?><h2 class="title"><?php print $title; ?></h2><?php } ?>

  <?php if ($tabs) { ?><div class="tabs"><?php print $tabs; ?></div><?php } ?>

  <?php print $help ?>
  <?php print $messages ?>

  <!-- start 'content' -->
  <?php print $content; ?>

  <!-- end 'content' -->

 </div><!--/#maincol-->

 <!--<br clear="all" />-->

 <div id="footer">
  <div id="footer-text">
   <?php print $footer_message ?>&nbsp;
  </div>
 </div>

</div><!--/#container-->
</div><!--/#drop-shadow-sides-->
<div id="drop-shadow-bottom">
 <!-- Thanks for any (optional) credit -->
 <div id="credit"><a href="http://nokoala.rosskendall.com/">No Koala!</a> theme by <a href="http://rosskendall.com/">Ross Kendall</a></div>
</div>
<?php print $closure ?>
</body>
</html>

