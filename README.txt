
NO KOALA! THEME
---------------

No Koala! Drupal theme by Ross Kendall - http://rosskendall.com.

The No Koala! theme is a simple but elegant single-column theme for 
the Drupal content management system.  It's goal is to focus on 
accessibility and usability, and should be suitable for a blog or
other simple websites.

The theme uses XHTML and CSS, and incorporates a CSS dropdown menu
that is designed to be integrated with the Drupal menu system.

INSTALLING
----------

Install in the same way as any other Drupal theme, uncompress the .tar.gz
file into the themes folder, and then enable the theme in the Drupal
administration.

Since the theme uses custom block regions, you will need to configure the
blocks appropriately. 

The 'top menu' region is only intended for the Primary Links menu.  In order
for the CSS dropdowns to work, you will need to ensure that the all of the
items in the menu are expanded.

There is a 'sidebar left' region, however this is not intended to be visible
to regular site visitors.  I use it for the Drupal navigation menu when
logged in as admin, and disable the recent posts item so that it is not
visible to those not logged in.

ISSUES
------

If you have any problems or suggestions, please use the drupal.org forums
to discuss the issue.

Please file any bug reports or feature requests as individual items using
the drupal.org issue system.  This page may be helpful:
http://drupal.org/contribute/testing

LICENSE
-------

Copyright (C) 2006  Ross Kendall - http://rosskendall.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

http://www.gnu.org/copyleft/gpl.html
