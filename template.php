<?php

/**
 * @file
 * Drupal template file for No Koala! theme by Ross Kendall.
 *
 * This file contains theme hooks that are designed to override default
 * theme functions.  It  is used with the phptemplate theme engine.
 */

/*
Copyright (C) 2006  Ross Kendall - http://rosskendall.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
http://www.gnu.org/copyleft/gpl.html
*/

/**
 * Defines custom block regions for No Koala theme.
 *
 * @return
 *   Returns an assosiative array containing names and descriptions for 
 *   defined block regions.
 */
function nokoala_regions() {
  return array(
      'content' => t('content'),
      'top_menu' => t('top menu'),
      'sidebar_left' => t('sidebar left (for admin)'),
      'footer_message' => t('footer message')
  );
}

?>
